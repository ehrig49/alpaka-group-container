# Copyright 2020 Simeon Ehrig
#
# ISC Software License


import argparse

import generator as gn


def parseArgs() -> argparse.Namespace:
    """Parse the input arguments.

    :returns: argparse.Namespace object
    :rtype: argparse.Namespace

    """
    parser = argparse.ArgumentParser(
        description="Generator of Dockerfiles for Alpka and Cupla.",
        formatter_class=argparse.RawTextHelpFormatter,
    )

    parser.add_argument(
        "--boost",
        type=str,
        nargs="+",
        help="Install specific boost versions.\n"
        "Versions: " + ", ".join(gn.boost_versions),
    )

    parser.add_argument(
        "--gcc",
        type=str,
        # if the argument is not use, the value is None
        # if the argument is used without assignment (--gcc) the value is an empty list
        default=None,
        nargs="*",
        choices=gn.gcc_versions,
        help="Install additional gcc compiler beside the system compiler.\n"
        "If only --gcc is passed without assignment, all available gcc will be installed.\n"
        "Versions: " + ", ".join(gn.gcc_versions),
    )

    parser.add_argument(
        "--clang",
        type=str,
        # if the argument is not use, the value is None
        # if the argument is used without assignment (--clang) the value is an empty list
        default=None,
        nargs="*",
        choices=gn.clang_versions,
        help="Install clang compiler.\n"
        "If only --clang is passed without assignment, all available gcc will be installed.\n"
        "Versions: " + ", ".join(gn.clang_versions),
    )

    parser.add_argument(
        "--ubuntu",
        type=float,
        help="Select Ubuntu base image. If not set, use Ubuntu 18.04.",
        choices=gn.ubuntu_versions.keys(),
    )

    parser.add_argument(
        "--cuda",
        type=float,
        help="Select CUDA base image and SDK version. The Ubuntu base image depends on the CUDA image.",
        choices=gn.cuda_versions.keys(),
    )

    parser.add_argument(
        "--rocm",
        type=float,
        help="Select rocm base image and SDK version. The Ubuntu base image is 20.04.",
        choices=gn.rocm_versions.keys(),
    )

    parser.add_argument(
        "--picongpu",
        action="store_true",
        help="Install all requirements for PIConGPU.",
    )

    parser.add_argument(
        "--version",
        type=str,
        help="Set a version which is store as CONTAINER_VERSION environment variable.",
    )

    return parser.parse_args()


def main():
    args = parseArgs()

    if args.boost:
        for b in args.boost:
            if b not in gn.boost_versions:
                print(b + " is not a supported boost version")
                exit(1)
        boost_version = args.boost
    else:
        boost_version = gn.boost_versions

    if args.gcc is None:
        gcc_versions = []
    elif len(args.gcc) == 0:
        # the argument was use without assignment -> --gcc
        # use all available versions
        gcc_versions = gn.gcc_versions
    else:
        gcc_versions = args.gcc

    if args.clang is None:
        clang_versions = []
    elif len(args.clang) == 0:
        # the argument was use without assignment -> --clang
        # use all available versions
        clang_versions = gn.clang_versions
    else:
        clang_versions = args.clang

    rg = gn.recipe_generator(
        used_boost_versions=boost_version,
        gcc_versions=gcc_versions,
        clang_versions=clang_versions,
        ubuntu_version=args.ubuntu,
        cuda_version=args.cuda,
        rocm_version=args.rocm,
        picongpu=args.picongpu,
        version=args.version,
    )

    print(rg.get_full_stack())


if __name__ == "__main__":
    main()
