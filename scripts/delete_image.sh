#!/bin/sh

REG_SHA256=ade837fc5224acd8c34732bf54a94f579b47851cc6a7fd5899a98386b782e228
REG_VERSION=0.16.1

echo "delete_script.sh, image -> $1"
echo "delete_script.sh, tag -> $2"

IMAGE_TO_DELET=$(echo "$1:$2")
/usr/local/bin/reg rm -d --auth-url $CI_REGISTRY -u $CI_REGISTRY_USER -p $CI_REGISTRY_PASSWORD $IMAGE_TO_DELET
